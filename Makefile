RESULTS = $(patsubst %.tex, %.pdf, $(wildcard den*.tex))

all: ${RESULTS}

redo: clear all clean

clean:
	rm -f *.aux *.log

clear:
	rm -f *.pdf *.aux *.log

den%.pdf: den%.tex kostra.tex clanky/*.tex
	pdflatex $<

